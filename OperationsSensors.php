<?php include 'header2.php'; ?>
<?php include 'action.php'; ?>
<?php include 'connection.php'; ?>
<!Doctype html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.css"/>
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
</head>
<title>Операції з датчиками</title>
<body>
    <div class="text">
    <h1 class="py-4 bg-dark text-light rounded" ><i class="fas fa-swatchbook"></i> Операції з даними</h1>
    </div>
<div class="container">
  <div class="row">
    <div class="col-lg-6">
      <h4>Інформація про датчики</h4>  
    </div>
    <div class="col-lg-6">
    <a href="ExportToExcel.php" class="btn btn-success m-1 float-right" title="Натисніть щоб експортувати">
    <i class="fa fa-download"></i> Експортувати в Excel</a>
    <button type="button" class="btn btn-primary m-1 float-right" data-toggle="modal" data-target="#addModal">
    <i class="fa fa-plus"></i> Додати новий запис</button>
    </div>
  </div><br>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="table-responsive" id="tableData">
        <h3 class="text-center text-success" style="margin-top: 150px;">Завантаження...</h3>
      </div>
    </div>
  </div>
</div>
 <script type="text/javascript" src="js/getData.js"></script>
<!-- Add Record  Modal -->
<div class="modal" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Додати новий запис</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="formData">
          <div class="form-group">
            <label for="Id">Id:</label>
            <input type="text" class="form-control" name="Id" placeholder="Введіть Id" value=1 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="Value">Кількість:</label>
            <input type="text" class="form-control" name="Value" placeholder="Введіть кількість" 
            required="">
          </div>
          <div class="form-group">
            <label for="UserId">Виберіть користувача:</label>
              <select id="LastName">
              <option value="">Користувач</option>
              <?php
              $sql = "SELECT * FROM users";
              $resultset = mysqli_query($con, $sql) 
              or die("database error:". mysqli_error($con));
              while( $rows = mysqli_fetch_assoc($resultset) ) { 
              ?>
              <option value="<?php echo $rows["UserId"]; ?>">
                <?php echo $rows["LastName"]; ?></option>
              <?php } ?>
              </select>
            <input type="hidden" class="form-control" name="UserId" id="user_id">
            <input type="text" class="form-control" name="ULS" id="ULastName" 
            placeholder="Виберіть користувача" required="">
          </div>
          <div class="form-group">
            <label for="cdate">Дата сигналу:</label>
            <input type="date" class="form-control" name="cdate" placeholder="Введіть дату отримання сигналу" required="">
          </div>
          <hr>
          <div class="form-group float-right">
            <button type="submit" class="btn btn-success" id="submit">Підтвердити</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Закрити</button>
          </div>  
        </form>
      </div>
    </div>
  </div>
</div>
 <script type="text/javascript" src="js/getData2.js"></script>
<!-- Update Record  Modal -->
<div class="modal" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Редагувати запис</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="EditformData" action="action.php" method="POST">
          <input type="hidden" name="id" id="edit-form-id">
          <div class="form-group">
            <label for="Id">Id:</label>
            <input type="text" class="form-control" name="SensorsId" id="Id" placeholder="Введіть Id" required="" readonly>
          </div>
          <div class="form-group">
            <label for="Value">Кількість:</label>
            <input type="text" class="form-control" name="SensorsValue" id="Value" placeholder="Введіть кількість" required="">
          </div>
             <div class="form-group">
            <label for="UserId">Виберіть користувача:</label>
              <select id="LastName1">
              <option value="" selected="selected">Користувач</option>
              <?php
              $sql1 = "SELECT * FROM users";
              $resultset1 = mysqli_query($con, $sql1) 
              or die("database error:". mysqli_error($con));
              while( $rows1 = mysqli_fetch_assoc($resultset1) ) { 
              ?>
              <option value="<?php echo $rows1["UserId"]; ?>">
                <?php echo $rows1["LastName"]; ?></option>
              <?php } ?>
              </select>
            <input type="hidden" class="form-control" name="SensorsUserId" id="UserId">
            <input type="text" class="form-control" name="SensorsLastName" id="LastName12"
            placeholder="Виберіть користувача" required="">
          </div>
          <div class="form-group">
            <label for="cdate">Дата сигналу:</label>
        <input type="date" class="form-control" name="Sensorscdate" id="cdate" placeholder="Введіть дату отримання сигналу" required="">
          </div>
          <hr>
          <div class="form-group float-right">
            <button type="submit" class="btn btn-primary" id="update1" name="update">Оновити запис</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Закрити</button>
          </div>  
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Delete Record  Modal -->
<div class="modal" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Видалити запис</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="DeleteformData" action="action.php" method="POST">
          <input type="hidden" name="id1" id="delete-form-id">
          <div class="form-group">
            <label for="Id">Id:</label>
            <input type="text" class="form-control" name="SensorsId1" id="Id1" 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="Value">Кількість:</label>
            <input type="text" class="form-control" name="SensorsValue1" id="Value1" 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="UserId">Користувач:</label>
            <input type="text" class="form-control" name="SensorsUserId1" id="UserId1" 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="cdate">Дата сигналу:</label>
        <input type="date" class="form-control" name="Sensorscdate1" id="cdate1" 
        required="" readonly>
          </div>
          <hr>
          <div class="form-group float-right">
            <button type="submit" class="btn btn-danger" id="delete1" name="delete">Видалити запис</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Закрити</button>
          </div>  
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.js">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
 <script type="text/javascript">
  $(document).ready(function(){
  ShowAllSensors();
  //View Record
  function ShowAllSensors(){
    $.ajax({
      url : "action.php",
      type: "POST",
      data : {action:"view"},
      success:function(response){
          $("#tableData").html(response);
          $("table").DataTable({
            order:[0, 'DESC']
          });
        }
      });
    }
    //insert ajax request data
    $("#submit").click(function(e){
        if ($("#formData")[0].checkValidity()) {
          e.preventDefault();
          $.ajax({
            url : "action.php",
            type : "POST",
            data : $("#formData").serialize()+"&action=insert",
            success:function(response){
              Swal.fire({
                icon: 'success',
                title: 'Новий запис успішно додано',
              });
              $("#addModal").modal('hide');
              $("#formData")[0].reset();
              ShowAllSensors();
              setTimeout(function() {
                location.reload();
                }, 999);
            }
          });
        }
    });
    //update ajax request data
    $("#update1").click(function(e){
        if ($("#EditformData")[0].checkValidity()) {
          e.preventDefault();
          $.ajax({
            url : "action.php",
            type : "POST",
            data : $("#EditformData").serialize()+"&action=update",
            success:function(response){
              Swal.fire({
                icon: 'success',
                title: 'Запис успішно оновлено',
              });
              $("#editModal").modal('hide');
              $("#EditformData")[0].reset();
              ShowAllSensors();
              setTimeout(function() {
                location.reload();
                }, 999);
            }
          });
        }
    });
    //delete ajax request data
    $("#delete1").click(function(e){
        if ($("#DeleteformData")[0].checkValidity()) {
          e.preventDefault();
          $.ajax({
            url : "action.php",
            type : "POST",
            data : $("#DeleteformData").serialize()+"&action=delete",
            success:function(response){
              Swal.fire({
                icon: 'success',
                title: 'Запис успішно видалено',
              });
              $("#deleteModal").modal('hide');
              $("#DeleteformData")[0].reset();
              ShowAllSensors();
              setTimeout(function() {
                location.reload();
                }, 999);
            }
          });
        }
    });
    //Select record for update
    $("body").on("click", ".editBtn", function(e){
      e.preventDefault();
      var editId = $(this).attr('Number');
      $.ajax({
        url : "action.php",
        type : "POST",
        data : {editId:editId},
        success:function(response){
          var data = JSON.parse(response);
          $("#edit-form-id").val(data.Number);
          $("#Id").val(data.Id);
          $("#Value").val(data.Value);
          $("#UserId").val(data.UserId);
          $("#LastName12").val(data.LastName);
          $("#cdate").val(data.cdate);
        }
      });
    });
    //Select record for delete
    $("body").on("click", ".deleteBtn", function(e){
      e.preventDefault();
      var deleteId = $(this).attr('Number');
      $.ajax({
        url : "action.php",
        type : "POST",
        data : {deleteId:deleteId},
        success:function(response){
          var data = JSON.parse(response);
          $("#delete-form-id").val(data.Number);
          $("#Id1").val(data.Id);
          $("#Value1").val(data.Value);
          $("#UserId1").val(data.LastName);
          $("#cdate1").val(data.cdate);
        }
      });
    });
  });
</script>
</body>
</html>
<div id="form" class="container-fluid">
  <?php include 'footer.php'; ?>
</div>