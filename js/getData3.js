$(document).ready(function(){  
	// code to get all records from table via select box
	$("#RoleName").change(function() {    
		var id = $(this).find(":selected").val();
		var dataString = 'roleid='+ id;    
		$.ajax({
			url: 'Selected2.php',
			dataType: "json",
			data: dataString,  
			cache: false,
			success: function(roleData) 
			{ 				
				 $("#role_id").val(roleData.RoleId);
				 $("#role_Name").val(roleData.RoleName);  	
			} 
		});
 	}) 
});