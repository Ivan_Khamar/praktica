$(document).ready(function(){  
	// code to get all records from table via select box
	$("#LastName").change(function() {    
		var id = $(this).find(":selected").val();
		var dataString = 'userid='+ id;    
		$.ajax({
			url: 'Selected.php',
			dataType: "json",
			data: dataString,  
			cache: false,
			success: function(userData) 
			{				
				$("#user_id").val(userData.UserId);
				$("#ULastName").val(userData.LastName);  	
			} 
		});
 	}) 
});
