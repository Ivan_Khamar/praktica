$(document).ready(function(){  
	// code to get all records from table via select box
	$("#RoleName1").change(function() {    
		var id = $(this).find(":selected").val();
		var dataString = 'roleid='+ id;    
		$.ajax({
			url: 'Selected2.php',
			dataType: "json",
			data: dataString,  
			cache: false,
			success: function(roleData) 
			{ 				
				 $("#role_id1").val(roleData.RoleId);
				 $("#role_Name1").val(roleData.RoleName);  	
			} 
		});
 	}) 
});