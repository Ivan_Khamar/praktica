<?php
	include_once('config.php');
	$dbObj = new Database();

	// Insert Record	
	if (isset($_POST['action']) && $_POST['action'] == "insert") 
	{
		$Id = $_POST['Id'];
		$Value = $_POST['Value'];
		$UserId = $_POST['UserId'];
		$cdate = $_POST['cdate'];
		$dbObj->insertRecord($Id, $Value, $UserId, $cdate);
	}

	// Update Record	
	 if(isset($_POST['action']) && $_POST['action'] == "update") 
	{
     $dbObj->updateRecord($_POST);
  	}

  	// Delete Record	
	 if(isset($_POST['action']) && $_POST['action'] == "delete") 
	{
      $dbObj->deleteRecord($_POST);
  	}

  	// Edit to Update Record	
	if (isset($_POST['editId'])) 
	{
		$editId = $_POST['editId'];
		$row = $dbObj->getRecordById($editId);
		echo json_encode($row);
	}

	// Edit to Delete Record	
	if (isset($_POST['deleteId'])) 
	{
		$deleteId = $_POST['deleteId'];
		$row = $dbObj->getRecordById($deleteId);
		echo json_encode($row);
	}  

	// View record
	if (isset($_POST['action']) && $_POST['action'] == "view") 
	{
		$output = "";
		$sensors = $dbObj->displayRecord();

		if ($dbObj->totalRowCount() > 0) {
			$output .="<table class='table table-striped table-hover'>
			        <thead>
			          <tr>
			            <th style='text-align: center; vertical-align: middle;'>Номер датчика</th>
			            <th style='text-align: center; vertical-align: middle;'>Id датчика</th>
			            <th style='text-align: center; vertical-align: middle;'>Кількість</th>
			            <th style='text-align: center; vertical-align: middle;'>Користувач</th>
			            <th>Дата сигналу</th>
			            <th>Дії</th>
			          </tr>
			        </thead>
			        <tbody>";
			foreach ($sensors as $sensor) {
			$output.="<tr>
			            <td style='text-align: center; vertical-align: middle;'>".$sensor['Number']."</td>
			            <td style='text-align: center; vertical-align: middle;'>".$sensor['Id']."</td>
			            <td style='text-align: center; vertical-align: middle;'>".$sensor['Value']."</td>
			            <td style='text-align: center; vertical-align: middle;'>".$sensor['LastName']." ".$sensor['FirstName']."</td>
			            <td>".date('d-M-Y', strtotime($sensor['cdate']))."</td>
			            <td>
			             <a href='#editModal' style='color:green' data-toggle='modal' 
			              class='editBtn' Number='".$sensor['Number']."'>
			              <i class='fa fa-pencil'></i></a>&nbsp;
			              <a href='#deleteModal' style='color:red' data-toggle='modal'  
			              class='deleteBtn' Number='".$sensor['Number']."'>
			              <i class='fa fa-trash' ></i></a>&nbsp;
			            </td>
			        </tr>";
				}
			$output .= "</tbody>
      		</table>";
      		echo $output;	
		}else
		{
			echo '<h3 class="text-center mt-5">Записів не знайдено</h3>';
		}
	}
?>