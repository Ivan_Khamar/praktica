<?php include 'ProfileServer.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <title>Профіль</title>
  <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
  <link href="https://unpkg.com/tabulator-tables@4.2.7/dist/css/tabulator.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/AnotherStyle.css">
</head>
<body>
	<main>
	<div id="main" class="maindiv">
	<header id="header" class="container-fluid">
    <div class="head">
    		<div class="btnexit">
	     <a href="MainMenu.php">
	       <button class="btn21"
	       style="margin-top: 5%;">Повернутися назад</button>
	     </a>
	    </div>
	 	<p><h1 class="h1">Профіль користувача</h1>
		<div id="border-bot" class="container"></div></p>
		</div>
		<form action="" method="post">
			<div class="ProfFrom">
			<div class="profileItems">
	<div id="loginform" class="form-group">
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Ім'я</label>
			</div>
			<div>
				<input class="form-control same-input" type="text" name="FirstName"
				value="<?php echo $fetchRow['FirstName']?>" required>
			</div>
		</div>
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Фамілія</label>
			</div>
			<div>
				<input class="form-control same-input" type="text" name="LastName"
				value="<?php echo $fetchRow['LastName']?>" required>
			</div>
		</div>
	</div>
		<div id="loginform" class="form-group">
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Еmail</label>
			</div>
			<div>
				<input class="form-control same-input" type="text" name="Email"
				value="<?php echo $fetchRow['Email']?>" required>
			</div>
		</div>
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Телефон</label>
			</div>
			<div>
				<input class="form-control same-input" type="text" name="Phone"
				value="<?php echo $fetchRow['Phone']?>" required>
			</div>
		</div>
	</div>
</div>
<div class="Empty12">
			<div id="loginform" class="form-group">
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Логін</label>
			</div>
			<div>
				<input class="form-control same-input" type="text" name="username" 
				value="<?php echo $fetchRow['username']?>" required>
			</div>
			<div class="container2">
			</div>
		</div>
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Старий пароль</label>
			</div>
			<div>
				<input id="vis" class="form-control same-input" type="password" name="passcode"
				value="<?php echo $fetchRow['passcode']?>" required>
			</div>
			<div class="container1">
			<i class="bi bi-eye-slash" id="togglePassword"></i>
			</div>
		</div>
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Новий пароль</label>
			</div>
			<div>
				<input id="vis2" class="form-control same-input" type="password" name="passcode1">
			</div>
			<div class="container1">
			<i class="bi bi-eye-slash" id="togglePassword2"></i>
			</div>
		</div>
			<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Підтвердіть пароль</label>
			</div>
			<div>
				<input id="vis3" class="form-control same-input" type="password" name="passcode2"/>
			</div>
			<div class="container1">
			<i class="bi bi-eye-slash" id="togglePassword3"></i>
			</div>
		</div>
	</div>
</div>
</div>
</div>
  <div id="content" class="container-fluid">
  <div style="margin-left: 45%;">
	<input class="btn22" name="update" type='submit' value="Зберегти зміни"></input>
  </div>
</div>
</form>
</header>
</div>
</main>
<script src="js/app2.js"></script>
</body>
</html>
<div id="form" class="container-fluid">
  <?php include 'Footer.php'; ?>
</div>