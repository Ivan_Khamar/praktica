<?php include 'header2.php'; ?>
<!DOCTYPE html>
<html>
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
<title>Головна сторінка</title>
<div class="rows">
  <div class="side">
    <h2>Про 2Y0A21</h2>
    <h5>Інфрачервоний датчик наближення</h5>
    <?php 
    $image_url='img\1.jpg';?>
    <img src="<?php echo $image_url;?>"
    style="margin-left: 30px;"
    width="300" height="300">
    <h3>Особливості</h3>
    <div class="color1" style="height:50px; width: 350px;">
    <p style="color:white; margin-left: 10px;">Діапазон вимірювання відстані: від 10-80 см</p></div><br>
    <div class="color1" style="height:50px; width: 350px;">
    <p style="color:white; margin-left: 10px;">Тип аналогового виходу</p></div><br>
    <div class="color1" style="height:50px; width: 350px;">
    <p style="color:white; margin-left: 10px;">Розмір упаковки: 29,5х13х13,5 мм</p></div><br>
    <div class="color1" style="height:50px; width: 350px;">
    <p style="color:white; margin-left: 10px;">Струм споживання: 30 мА</p></div><br>
    <div class="color1" style="height:50px; width: 350px;">
    <p style="color:white; margin-left: 10px;">Напруга живлення: від 4,5 до 5,5 В</p></div>
    </div>
    <div class="main">
    <h2>Опис датчика</h2>
    <h5>2Y0A21</h5>
    <div class="color1" style="height:150px; width: 100%;">
    <p style="color:white; margin-left: 10px;">
    GP2Y0A21YK0F - це датчик вимірювання відстані, що складається з інтегрованої комбінації PSD (чутливого до положення детектора), IRED (інфрачервоного випромінювального діода) та схеми обробки сигналів. На різноманітність відбивної здатності об’єкта, температуру навколишнього середовища та тривалість роботи неможливо легко вплинути на виявлення відстані через застосування методу тріангуляції. Цей пристрій виводить напругу, що відповідає відстані виявлення. Отже, цей датчик також може використовуватися як датчик наближення.</p>
    </div><br>
    <h2>Сфери застосування</h2>
    <h5>2Y0A21</h5>
    <div class="color1" style="height: 50px; width: 650px; margin-left: 100px;">     
    <p style="color:white; margin-left: 30px;">Сенсорний перемикач (сантехнічне обладнання, контроль освітленості)</p>
    </div><br>
    <div class="color1" style="height: 50px; width: 650px; margin-left: 100px;">
    <p style="color:white; margin-left: 30px;">Робот-пилосос</p>
    </div><br>
    <div class="color1" style="height: 50px; width: 650px; margin-left: 100px;">
    <p style="color:white; margin-left: 30px;">Датчик енергозбереження (банкомат, торговий автомат)</p>
    </div><br>
    <div class="color1" style="height: 50px; width: 650px; margin-left: 100px;">
    <p style="color:white; margin-left: 30px;">Обладнання для розваг (робот, аркадна ігрова машина)</p>
    </div><br>
    <?php 
    $image_url='img\4.jpeg';?>
    <img src="<?php echo $image_url;?>"
    style="margin-left: 200px;">
  </div>
</div>
<div id="form" class="container-fluid">
  <?php include 'footer.php'; ?>
</div>
</html>