<?php 
    session_start();
    require_once 'connection.php';
    if (!empty($_GET["id"])){
        $id=$_GET["id"];
    } 
    else {
        $id=1;
    }
    $userid=$_SESSION['userId'];
    $conn = mysqli_connect($host, $user, $password, $database);
    if(!$conn)
    {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "Select * From users Where UserId=$userid";
    $rs = mysqli_query($conn, $sql);
    $fetchRow = mysqli_fetch_assoc($rs);
    $passwordSalt = "password";
    $fetchRow['username'] = openssl_decrypt($fetchRow['username'],"AES-128-ECB",$passwordSalt);
    $fetchRow['passcode'] = openssl_decrypt($fetchRow['passcode'],"AES-128-ECB",$passwordSalt);
?>
<?php 
    if(count($_POST)>0) {
     $mylogin = mysqli_real_escape_string($conn,$_POST['username']);
     $passwordSalt="password";
     $login_to_encrypt=$mylogin;
     $encrypted_Login=openssl_encrypt($login_to_encrypt,"AES-128-ECB",$passwordSalt);
     $sql = "Update users Set FirstName='" . $_POST["FirstName"] . "', LastName='" . $_POST["LastName"] . "', Email='" . $_POST["Email"] . "', Phone='" . $_POST["Phone"] . "', username='" . $encrypted_Login . "' WHERE UserId= $userid";
     mysqli_query($conn,$sql);
     header("Refresh:0");
    }
?>
<?php 
   if(!empty($_POST["passcode"])&!empty($_POST["passcode1"])&!empty($_POST["passcode2"]))
   { 
      $mypassword = mysqli_real_escape_string($conn,$_POST['passcode']);
      $mypassword1 = mysqli_real_escape_string($conn,$_POST['passcode1']); 
      $mypassword2 = mysqli_real_escape_string($conn,$_POST['passcode2']);
      if($mypassword1!=$mypassword2) {
        $message = "Passwords different";
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
      else
      {
      $passwordSalt="password";
      $pass_to_encrypt=$mypassword2;
      $encrypted_Pass=openssl_encrypt($pass_to_encrypt,"AES-128-ECB",$passwordSalt);
      $sql = "Update users Set passcode='" . $encrypted_Pass . "' WHERE UserId= $userid";
      $result = mysqli_query($conn,$sql);
      header("Refresh:0");
    }
   }
?>