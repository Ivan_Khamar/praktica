<?php include 'header.php'; ?>
<!DOCTYPE html>
<html>
<title>Вхід</title>
<head>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" 
   href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/AnotherStyle.css">
</head>
<div id="nav">
   <div class='logbutt'>
      <button class="btn21" type='button'>Увійти</button>
   </div>
   <div class='logbutt'>
      <a href="Registrate.php">
      <button  class="btn21" type='button'>Зареєструватись</button>
      </a>
   </div>
</div>
<body>
<main>
<form action="log/login.php" method="post">
	<div id="loginform" class="form-group">
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Логін</label>
			</div>
			<div>
				<input class="form-control same-input" type="text" name="username" required="">
			</div>
         <div class="container2">
         </div>
		</div>
		<div class="logrows">
			<div class="labelforinput">	
				<label class="llog" for="inputrec">Пароль</label>
			</div>
			<div>
			<input id="passForLogIn" class="form-control same-input password" type="password" 
         name="password" required="">
			</div>
         <div class="container1">
         <i class="bi bi-eye-slash" id="togglePassword4"></i>
         </div>
		</div>
      <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Повторіть пароль</label>
         </div>
         <div>
            <input id="passForLogIn2" class="form-control same-input password" type="password" 
            name="password2" required="">
         </div>
         <div class="container1">
         <i class="bi bi-eye-slash" id="togglePassword5"></i>
         </div>
      </div>
		<div class="logrows">
		<input class="btn btn-outline-secondary inp rig temp1" type='submit' value="Увійти" 
      	name="logInButton">
		</div>
	</div>
</form>
</main>
<script src="js/app3.js"></script>
</body>
</html>
<?php include 'footer1.php'; ?>