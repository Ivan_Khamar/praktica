<?php include 'header2.php'; ?>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
<title>Статистика датчиків</title>
<div class="text">
  	<h1>Таблиця статистики датчиків</h1>
  	</div>
 <div id="mytabl"></div>
 <div class="text">
  	<h1>Графік статистики датчиків</h1>
  	</div>
<div id="chart-conteiner">
    <canvas id="mycanvas"></canvas>
</div>
<form id="form" class="container-fluid" action="Getjson.php">
    <div id="flex-wrap" class="form-group">
		<div class="rows">
			<div class="labelforinput">
				<label for="inputdatea">Початкова дата</label>
			</div>
			<div>
			 <input id="inputdatea" class="form-control same-input" type='date' name='datea' onclick="uncheckIfDate()">
			</div>
		</div>
		<div class="rows">
			<div class="labelforinput">
				<label for="inputdateb">Кінцева дата</label>
			</div>
			<div>	
			<input id="inputdateb" class="form-control same-input" type='date' name='dateb' onclick="uncheckIfDate()">
			</div>
		</div>
	</div>
    <div id="forradio" class="form-group">
		<div class="form-check form-check-inline">
			<input id="rFilW" class="form-check-input" type="radio" name="rFilter" value="Week">
			<label class="form-check-label" for="rFilW">Тиждень</label>
		</div>
		<div class="form-check form-check-inline">
			<input id="rFilM" class="form-check-input" type="radio" name="rFilter" value="Month">
			<label class="form-check-label" for="rFilM">Місяць</label>
		</div>
		<div class="form-check form-check-inline">
			<input id="rFilY" class="form-check-input" type="radio" name="rFilter" value="Year">
			<label class="form-check-label" for="rFilY">Рік</label>
		</div>
	</div>
    <div id="requestbutton"><button id="confirm" class="btn btn-outline-secondary" type="button"  onclick="getChart()">Ok</button></div>
</form>  
</div>
<?php include 'footer1.php'; ?>