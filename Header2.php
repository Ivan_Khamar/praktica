<?php 
  session_start();
  require_once 'connection.php';
    if (!empty($_GET["id"])){
        $id=$_GET["id"];
    } 
    else {
        $id=1;
    }
    $userid=$_SESSION['userId'];
    $conn = mysqli_connect($host, $user, $password, $database);
    if(!$conn)
    {
        die("Connection failed: " . mysqli_connect_error());
    }
    $sql = "SELECT RoleId
    FROM users WHERE UserId=$userid";
    $rs = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($rs);
?>
<!DOCTYPE html>
<html lang="ua">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/AnotherStyle.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
  <link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
  <link href="https://unpkg.com/tabulator-tables@4.2.7/dist/css/tabulator.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="header54">
      <div class="divempty">
      </div>
      <div class="divtext">
    <h1>Статистика датчиків</h1>
    <p> <b>Ми</b> надаємо інформацію про ваші датчики</p>
    </div>
    <div class="divempty111">
      </div>
     <a href="Profile.php">
       <button class="btn20"><i class="fa fa-user-circle"></i> Профіль</button>
     </a>
      <?php 
        if(!empty($_SESSION))
        if(!empty($_SESSION["login_user"])) { 
          $way='log/logout.php';
          echo "<form action='$way'>";
          echo "<button class='btn20' type='submit'>Вийти</button>";
          echo '</form>';
        }
      ?>
    </div> 
  <div class="navbar">
  <a href="MainMenu.php">Головна сторінка</a>
  <a href="GetAll.php">Статистика датчиків</a>
   <?php 
    if(($row['RoleId'] == 1) || $row['RoleId'] == 2 ){
      ?>
  <a href="OperationsSensors.php">Операції з датчиками</a>
   <?php }
    ?>
    <?php 
    if(($row['RoleId'] == 1)){
      ?>
  <a href="ManagementPersonal.php">Менеджмент користувачів</a>
  <?php }
    ?>
  <a href="AboutUs.php">Про нас</a>
  </div>
  </body>
  </html>