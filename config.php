<?php
	class Database 
	{
		private $servername = "localhost";
		private $username   = "root";
		private $password   = "";
		private $dbname = "db";
		public $con;

		public function __construct()
		{
			try {
			 $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}

		// Insert
		public function insertRecord($Id, $Value, $UserId, $cdate)
		{
		 $sql = "INSERT INTO sensors (Id, Value, UserId, cdate) 
		 VALUES('$Id','$Value','$UserId','$cdate')";
			$query = $this->con->query($sql);
			if ($query) {
				return true;
			}else{
				return false;
			}
		}

		// Update customer data into customer table
		public function updateRecord($postData)
		{
			$Id = $this->con->real_escape_string($_POST['SensorsId']);
			$Value = $this->con->real_escape_string($_POST['SensorsValue']);
			$UserId = $this->con->real_escape_string($_POST['SensorsUserId']);
			$cdate = $this->con->real_escape_string($_POST['Sensorscdate']);
			$id = $this->con->real_escape_string($_POST['id']);
			if (!empty($id) && !empty($postData)) {
			$query = "UPDATE sensors SET Id = '$Id', Value = '$Value', UserId = '$UserId',
			cdate = '$cdate' WHERE Number = '$id'";
			$sql = $this->con->query($query);
			}
		}

		// Delete customer data from customer table
		public function deleteRecord($getData)
		{
			$id1 = $this->con->real_escape_string($_POST['id1']);
			$query = "DELETE FROM sensors WHERE Number = '$id1'";
			$sql = $this->con->query($query);
		}

		// Fetch records for show listing
		public function displayRecord()
		{
			$sql = "SELECT s.Number, s.Id, s.Value, s.cdate, s.UserId, u.UserId, u.LastName, u.FirstName  
    		FROM sensors as s INNER JOIN users as u ON s.UserId=u.UserId";
			$query = $this->con->query($sql);
			$data = array();
			if ($query->num_rows > 0) {
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				return $data;
			}else{
				return false;
			}
		}

		// Fetch single data for edit
		public function getRecordById($Number)
		{
			$query = "SELECT s.Number, s.Id, s.Value, s.cdate, s.UserId, u.UserId, u.LastName, u.FirstName  
    		FROM sensors as s INNER JOIN users as u ON s.UserId=u.UserId 
    		WHERE Number = '$Number'";
			$result = $this->con->query($query);
			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				return $row;
			}else{
				return false;
			}
		}

		public function totalRowCount()
		{
			$sql = "SELECT s.Number, s.Id, s.Value, s.cdate, s.UserId, u.UserId, u.LastName, u.FirstName  
    		FROM sensors as s INNER JOIN users as u ON s.UserId=u.UserId";
			$query = $this->con->query($sql);
			$rowCount = $query->num_rows;
			return $rowCount;
		}
	}
?>