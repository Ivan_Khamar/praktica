<?php include 'header2.php'; ?>
<?php include 'action2.php'; ?>
<?php include 'connection.php'; ?>
<!Doctype html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.css"/>
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
</head>
<title>Менеджмент користувачів</title>
<body>
    <div class="text">
    <h1 class="py-4 bg-dark text-light rounded" ><i class="fas fa-swatchbook"></i> Операції з даними</h1>
    </div>
<div class="container">
  <div class="row">
    <div class="col-lg-6">
      <h4>Інформація про користувачів</h4>  
    </div>
    <div class="col-lg-6">
    <button type="button" class="btn btn-primary m-1 float-right" data-toggle="modal" 
    data-target="#addModal">
    <i class="fa fa-plus"></i> Додати нового користувача</button>
    </div>
  </div><br>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="table-responsive" id="tableData">
        <h3 class="text-center text-success" style="margin-top: 150px;">Завантаження...</h3>
      </div>
    </div>
  </div>
</div>
 <script type="text/javascript" src="js/getData3.js"></script>
<!-- Add Record  Modal -->
<div class="modal" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Додати нового користувача</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="formData">
          <div class="form-group">
            <label for="Id">Ім'я:</label>
            <input type="text" class="form-control" name="FirstName" 
            placeholder="Введіть ім'я користувача" 
            required="">
          </div>
          <div class="form-group">
            <label for="Value">Фамілія:</label>
            <input type="text" class="form-control" name="LastName" 
            placeholder="Введіть фамілію користувача" 
            required="">
          </div>
           <div class="form-group">
            <label for="cdate">Email:</label>
            <input type="email" class="form-control" name="Email" 
            placeholder="Введіть email користувача" 
            required="">
          </div>
          <div class="form-group">
            <label for="cdate">Телефон:</label>
            <input type="text" class="form-control" name="Phone" 
            placeholder="Введіть номер телефону користувача" required="">
          </div>
           <div class="form-group">
            <label for="cdate">Логін:</label>
            <input type="text" class="form-control" name="username" 
            placeholder="Введіть логін користувача" required="">
          </div>
           <div class="form-group">
            <label for="cdate">Пароль:</label>
            <input type="text" class="form-control" name="passcode" 
            placeholder="Введіть пароль користувача" required="">
          </div>
          <div class="form-group">
            <label for="UserId">Виберіть роль для користувача:</label>
              <select id="RoleName">
              <option value="">Роль</option>
              <?php
              $sql = "SELECT * FROM role";
              $resultset = mysqli_query($con, $sql) 
              or die("database error:". mysqli_error($con));
              while( $rows = mysqli_fetch_assoc($resultset) ) { 
              ?>
              <option value="<?php echo $rows["RoleId"]; ?>">
                <?php echo $rows["RoleName"]; ?></option>
              <?php } ?>
              </select>
            <input type="hidden" class="form-control" name="RoleId" id="role_id">
            <input type="text" class="form-control" name="RoleName" id="role_Name" 
            placeholder="Виберіть роль для користувача" required="">
          </div>
          <hr>
          <div class="form-group float-right">
            <button type="submit" class="btn btn-success" id="submit">Підтвердити</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Закрити</button>
          </div>  
        </form>
      </div>
    </div>
  </div>
</div>
 <script type="text/javascript" src="js/getData4.js"></script>
<!-- Update Record  Modal -->
<div class="modal" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Редагувати користувача</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="EditformData" action="action2.php" method="POST">
          <input type="hidden" name="id" id="edit-form-id1">
          <div class="form-group">
            <label for="Id">Ім'я:</label>
            <input type="text" class="form-control" name="FirstName1" 
            id="FirstName" required="" readonly>
          </div>
          <div class="form-group">
            <label for="Value">Фамілія:</label>
            <input type="text" class="form-control" name="LastName1" 
            id="LastName" required="" readonly>
          </div>
           <div class="form-group">
            <label for="cdate">Email:</label>
            <input type="email" class="form-control" name="Email1" 
            id="Email" required="" readonly>
          </div>
          <div class="form-group">
            <label for="cdate">Телефон:</label>
            <input type="text" class="form-control" name="Phone1" 
            id="Phone" required="" readonly>
          </div>
          <div class="form-group">
            <label for="UserId">Виберіть роль для користувача:</label>
              <select id="RoleName1">
              <option value="">Роль</option>
              <?php
              $sql = "SELECT * FROM role";
              $resultset = mysqli_query($con, $sql) 
              or die("database error:". mysqli_error($con));
              while( $rows = mysqli_fetch_assoc($resultset) ) { 
              ?>
              <option value="<?php echo $rows["RoleId"]; ?>">
                <?php echo $rows["RoleName"]; ?></option>
              <?php } ?>
              </select>
            <input type="hidden" class="form-control" name="RoleId1" id="role_id1">
            <input type="text" class="form-control" name="RoleName1" id="role_Name1" 
            placeholder="Виберіть роль для користувача" required="">
          </div>
          <hr>
          <div class="form-group float-right">
            <button type="submit" class="btn btn-primary" id="update" name="update1">Оновити дані користувача</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Закрити</button>
          </div>  
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Delete Record  Modal -->
<div class="modal" id="deleteModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Деактивазація користувача</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form id="DeleteformData" action="action.php" method="POST">
          <input type="hidden" name="id1" id="delete-form-id">
          <div class="form-group">
            <label for="Id">Ім'я:</label>
            <input type="text" class="form-control" name="SensorsId1" id="FirstName2" 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="Value">Фамілія:</label>
            <input type="text" class="form-control" name="SensorsValue1" id="LastName2" 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="UserId">Email:</label>
            <input type="email" class="form-control" name="SensorsUserId1" id="Email2" 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="UserId">Телефон:</label>
            <input type="text" class="form-control" name="SensorsUserId1" id="Phone2" 
            required="" readonly>
          </div>
          <div class="form-group">
            <label for="cdate">Роль:</label>
        <input type="text" class="form-control" name="Sensorscdate1" id="Role2" 
        required="" readonly>
          </div>
          <hr>
          <div class="form-group float-right">
            <button type="submit" class="btn btn-danger" id="delete1" name="delete">Деактивувати користувача</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Закрити</button>
          </div>  
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.22/datatables.min.js">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
 <script type="text/javascript">
  $(document).ready(function(){
  ShowAllSensors();
  //View Record
  function ShowAllSensors(){
    $.ajax({
      url : "action2.php",
      type: "POST",
      data : {action:"view"},
      success:function(response){
          $("#tableData").html(response);
          $("table").DataTable({
            order:[0, 'DESC']
          });
        }
      });
    }
    //insert ajax request data
    $("#submit").click(function(e){
        if ($("#formData")[0].checkValidity()) {
          e.preventDefault();
          $.ajax({
            url : "action2.php",
            type : "POST",
            data : $("#formData").serialize()+"&action=insert",
            success:function(response){
              Swal.fire({
                icon: 'success',
                title: 'Новий користувача успішно додано',
              });
              $("#addModal").modal('hide');
              $("#formData")[0].reset();
              ShowAllSensors();
              setTimeout(function() {
                location.reload();
                }, 999);
            }
          });
        }
    });
    //update ajax request data
    $("#update").click(function(e){
        if ($("#EditformData")[0].checkValidity()) {
          e.preventDefault();
          $.ajax({
            url : "action2.php",
            type : "POST",
            data : $("#EditformData").serialize()+"&action=update",
            success:function(response){
              Swal.fire({
                icon: 'success',
                title: 'Дані користувача успішно оновлено',
              });
              $("#editModal").modal('hide');
              $("#EditformData")[0].reset();
              ShowAllSensors();
              setTimeout(function() {
                location.reload();
                }, 999);
            }
          });
        }
    });
    //delete ajax request data
    $("#delete1").click(function(e){
        if ($("#DeleteformData")[0].checkValidity()) {
          e.preventDefault();
          $.ajax({
            url : "action2.php",
            type : "POST",
            data : $("#DeleteformData").serialize()+"&action=delete",
            success:function(response){
              Swal.fire({
                icon: 'success',
                title: 'Запис успішно видалено',
              });
              $("#deleteModal").modal('hide');
              $("#DeleteformData")[0].reset();
              ShowAllSensors();
              setTimeout(function() {
                location.reload();
                }, 999);
            }
          });
        }
    });
    //Select record for update
    $("body").on("click", ".editBtn", function(e){
      e.preventDefault();
      var editId = $(this).attr('UserId');
      $.ajax({
        url : "action2.php",
        type : "POST",
        data : {editId:editId},
        success:function(response){
          var data = JSON.parse(response);
          $("#edit-form-id1").val(data.UserId);
          $("#FirstName").val(data.FirstName);
          $("#LastName").val(data.LastName);
          $("#Email").val(data.Email);
          $("#Phone").val(data.Phone);
          $("#role_id1").val(data.RoleId);
          $("#role_Name1").val(data.RoleName);
        }
      });
    });
    //Select record for delete
    $("body").on("click", ".deleteBtn", function(e){
      e.preventDefault();
      var deleteId = $(this).attr('UserId');
      $.ajax({
        url : "action2.php",
        type : "POST",
        data : {deleteId:deleteId},
        success:function(response){
          var data = JSON.parse(response);
          $("#delete-form-id").val(data.UserId);
          $("#FirstName2").val(data.FirstName);
          $("#LastName2").val(data.LastName);
          $("#Email2").val(data.Email);
          $("#Phone2").val(data.Phone);
          $("#Role2").val(data.RoleName);
        }
      });
    });
  });
</script>
</body>
</html>
<div id="form" class="container-fluid">
  <?php include 'footer.php'; ?>
</div>