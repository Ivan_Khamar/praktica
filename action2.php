<?php 
   require_once $_SERVER['DOCUMENT_ROOT'].'/SensorCP/connection.php';
?>
<?php
	include_once('config2.php');
	$dbObj = new Database();

	// Insert Role	
	if (isset($_POST['action']) && $_POST['action'] == "insertRole") 
	{
		$RoleName = $_POST['RoleName'];
		$dbObj->insertRole($RoleName);
	}

	// Insert Record	
	if (isset($_POST['action']) && $_POST['action'] == "insert") 
	{
		if($_SERVER["REQUEST_METHOD"] == "POST") {
		$db = mysqli_connect($host, $user, $password, $database) 
        or die("Ошибка " . mysqli_error($link));

		$FirstName = $_POST['FirstName'];
		$LastName = $_POST['LastName'];
		$Email = $_POST['Email'];
		$Phone = $_POST['Phone'];
		$RoleId = $_POST['RoleId'];

		$myusername = mysqli_real_escape_string($db,$_POST['username']);
      	$mypassword = mysqli_real_escape_string($db,$_POST['passcode']); 

		$userName_to_encrypt=$myusername;
      	$passwordSalt="password";
      	$encrypted_UserName=openssl_encrypt($userName_to_encrypt,"AES-128-ECB",$passwordSalt);
      	$pass_to_encrypt=$mypassword;
      	$encrypted_Pass=openssl_encrypt($pass_to_encrypt,"AES-128-ECB",$passwordSalt);

		$dbObj->insertRecord1($FirstName, $LastName, $Email, $Phone, 
		$encrypted_UserName, $encrypted_Pass, $RoleId);
		}
	}

	// Update Record	
	 if(isset($_POST['action']) && $_POST['action'] == "update") 
	{
     $dbObj->updateRecord1($_POST);
  	}

  	// Delete Record	
	 if(isset($_POST['action']) && $_POST['action'] == "delete") 
	{
      $dbObj->deleteRecord1($_POST);
  	}

  	// Edit to Update Record	
	if (isset($_POST['editId'])) 
	{
		$editId = $_POST['editId'];
		$row = $dbObj->getRecordById1($editId);
		echo json_encode($row);
	}

	// Edit to Delete Record	
	if (isset($_POST['deleteId'])) 
	{
		$deleteId = $_POST['deleteId'];
		$row = $dbObj->getRecordById1($deleteId);
		echo json_encode($row);
	}  

	// View record
	if (isset($_POST['action']) && $_POST['action'] == "view") 
	{
		$output = "";
		$users = $dbObj->displayRecord1();

		if ($dbObj->totalRowCount1() > 0) {
			$output .="<table class='table table-striped table-hover'>
			        <thead>
			          <tr>
			            <th style='text-align: center; vertical-align: middle;'>Користувач</th>
			            <th style='text-align: center; vertical-align: middle;'>Email</th>
			            <th style='text-align: center; vertical-align: middle;'>Номер телефону</th>
			            <th style='text-align: center; vertical-align: middle;'>Роль</th>
			            <th>Дії</th>
			          </tr>
			        </thead>
			        <tbody>";
			foreach ($users as $user) {
			$output.="<tr>
			            <td style='text-align: center; vertical-align: middle;'>".$user['LastName']." ".$user['FirstName']."</td>
			            <td style='text-align: center; vertical-align: middle;'>".$user['Email']."</td>
			            <td style='text-align: center; vertical-align: middle;'>".$user['Phone']."</td>
			            <td style='text-align: center; vertical-align: middle;'>".$user['RoleName']."</td>
			            <td>
			              <a href='#editModal' style='color:green' data-toggle='modal' 
			              class='editBtn' UserId='".$user['UserId']."'>
			              <i class='fa fa-pencil'></i></a>&nbsp;
			              <a href='#deleteModal' style='color:red' data-toggle='modal'  
			              class='deleteBtn' UserId='".$user['UserId']."'>
			              <i class='fa fa-trash' ></i></a>&nbsp;
			            </td>
			        </tr>";
				}
			$output .= "</tbody>
      		</table>";
      		echo $output;	
		}else
		{
			echo '<h3 class="text-center mt-5">Записів не знайдено</h3>';
		}
	}
?>