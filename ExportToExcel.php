<?php 
// Include the database config file 
require_once 'connection.php'; 
 
// Filter the excel data 
function filterData(&$str){ 
    $str = preg_replace("/\t/", "\\t", $str); 
    $str = preg_replace("/\r?\n/", "\\n", $str); 
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
} 
 
// Excel file name for download 
$fileName = "Статистика датчиків " . date('d-m-Y') . ".xlsx"; 
 
// Column names 
$fields = array('Номер запису', 'Id датчика', 'Кількість', 'Фамілія', "Ім'я", 'Дата сигналу'); 
 
// Display column names as first row 
$excelData = implode("\t", array_values($fields)) . "\n"; 
 
// Get records from the database 
$query = $con->query("SELECT s.Number, s.Id, s.Value, s.cdate, u.LastName, u.FirstName  
    FROM sensors as s INNER JOIN users as u ON s.UserId=u.UserId
    ORDER BY s.cdate"); 
if($query->num_rows > 0){ 
    // Output each row of the data 
    $i=0; 
    while($row = $query->fetch_assoc()){ $i++; 
     $rowData = array($row['Number'], $row['Id'], $row['Value'], $row['LastName'], $row['FirstName'], 
     $row['cdate']); 
     array_walk($rowData, 'filterData'); 
     $excelData .= implode("\t", array_values($rowData)) . "\n"; 
    } 
}else{ 
    $excelData .= 'Записів не знайдено...'. "\n";    
} 
 
// Headers for download 
header("Content-Disposition: attachment; filename=\"$fileName\""); 
header("Content-Type: application/vnd.ms-excel"); 

// Render excel data 
echo $excelData; 

exit;