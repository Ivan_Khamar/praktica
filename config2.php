<?php
	class Database 
	{
		private $servername = "localhost";
		private $username   = "root";
		private $password   = "";
		private $dbname = "db";
		public $con;

		public function __construct()
		{
			try {
			 $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}

		// Insert user
		public function insertRecord1($FirstName, $LastName, $Email, $Phone, $username, $passcode, $RoleId)
		{
		 $sql = "INSERT INTO users (FirstName, LastName, Email, Phone, username, passcode, RoleId) 
		 VALUES('$FirstName','$LastName','$Email','$Phone','$username','$passcode','$RoleId')";
			$query = $this->con->query($sql);
			if ($query) {
				return true;
			}else{
				return false;
			}
		}

		// Update customer data into customer table
		public function updateRecord1($postData)
		{
			$RoleId = $this->con->real_escape_string($_POST['RoleId1']);
			$id = $this->con->real_escape_string($_POST['id']);
			if (!empty($id) && !empty($postData)) {
			$query = "UPDATE users SET RoleId = '$RoleId' WHERE UserId = '$id'";
			$sql = $this->con->query($query);
			}
		}

		// Delete customer data from customer table
		public function deleteRecord1($getData)
		{
			$id1 = $this->con->real_escape_string($_POST['id1']);
			$query = "DELETE FROM users WHERE UserId = '$id1'";
			$sql = $this->con->query($query);
		}

		// Fetch records for show listing
		public function displayRecord1()
		{
			$sql = "SELECT u.UserId, u.FirstName, u.LastName, u.Email, u.Phone, u.username, u.passcode,
			u.RoleId, r.RoleId, r.RoleName 
    		FROM users as u INNER JOIN role as r ON u.RoleId=r.RoleId";
			$query = $this->con->query($sql);
			$data = array();
			if ($query->num_rows > 0) {
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				return $data;
			}else{
				return false;
			}
		}

		// Fetch single data for edit
		public function getRecordById1($UserId)
		{
			$query = "SELECT u.UserId, u.FirstName, u.LastName, u.Email, u.Phone, u.username, u.passcode,
			u.RoleId, r.RoleId, r.RoleName 
    		FROM users as u INNER JOIN role as r ON u.RoleId=r.RoleId
    		WHERE UserId = '$UserId'";
			$result = $this->con->query($query);
			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				return $row;
			}else{
				return false;
			}
		}

		public function totalRowCount1()
		{
			$sql = "SELECT u.UserId, u.FirstName, u.LastName, u.Email, u.Phone, u.username, u.passcode,
			u.RoleId, r.RoleId, r.RoleName 
    		FROM users as u INNER JOIN role as r ON u.RoleId=r.RoleId";
			$query = $this->con->query($sql);
			$rowCount = $query->num_rows;
			return $rowCount;
		}
	}
?>