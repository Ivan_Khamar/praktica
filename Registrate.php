<?php include 'header.php'; ?>
<!DOCTYPE html>
<html>
<title>Реєстрація</title>
<head>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" 
   href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css" />
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/AnotherStyle.css">
</head>
<div id="nav">
   <div class='logbutt'>
      <a href="index.php">
      <button  class="btn21" type='button'>Увійти</button>
      </a>
   </div>
   <div class='logbutt'>
      <button  class="btn21" type='button'>Зареєструватись</button>
   </div>
</div>
<body>
<main>
<form action="log/registrate.php" method="post">
   <div id="loginform" class="form-group">
        <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Ім'я</label>
         </div>
         <div>
            <input class="form-control same-input" type="text" name="firstName" required="">
         </div>
         <div class="container2">
         </div>
      </div>
        <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Фамілія</label>
         </div>
         <div>
            <input class="form-control same-input" type="text" name="lastName" required="">
         </div>
         <div class="container2">
         </div>
      </div>
        <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Email</label>
         </div>
         <div>
            <input class="form-control same-input" type="email" name="email" required="">
         </div>
         <div class="container2">
         </div>
      </div>
        <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Телефон</label>
         </div>
         <div>
            <input class="form-control same-input" type="text" name="phone" required="">
         </div>
         <div class="container2">
         </div>
      </div>
      <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Логін</label>
         </div>
         <div>
            <input class="form-control same-input" type="text" name="username" required="">
         </div>
         <div class="container2">
         </div>
      </div>
      <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Пароль</label>
         </div>
         <div>
            <input id="passForReg1" class="form-control same-input" type="password" name="password" required="">
         </div>
         <div class="container1">
         <i class="bi bi-eye-slash" id="togglePass1"></i>
         </div>
      </div>
     <div class="logrows">
         <div class="labelforinput">   
            <label class="llog" for="inputrec">Повторіть пароль</label>
         </div>
         <div>
            <input id="passForReg2" class="form-control same-input" type="password" name="password2" required="">
         </div>
         <div class="container1">
         <i class="bi bi-eye-slash" id="togglePass2"></i>
         </div>
      </div>
      <div class="logrows">
         <input class="btn btn-outline-secondary temp1" type='submit' value="Зареєструватись">
      </div>
   </div>
</form>
</main>
<script src="js/app4.js"></script>
</body>
</html>
<?php include 'footer1.php'; ?>